(function($) {

    Drupal.behaviors.entityreference_selectall = {
        attach: function (context, settings) {


            $('input.entityreference_selectall').bind('click', function () {
                var checked = $(this).is(":checked");

                // select all options.
                $(this).closest('.form-wrapper').find('option').attr('selected', checked);

                // disable the select field, to prevent confusion.
                $(this).closest('.form-wrapper').find('select').attr("disabled", checked);

                // except the _none_ item, if available. Never select _none_.
                $(this).closest('.form-wrapper').find('option[value="_none"]').attr('selected', false);
            });


            $('#edit-field-cardinality').change(function() {

                // only show the checkbox when the cardinality of the field is set to unlimited.
                if($('#edit-field-cardinality').val() == -1) {
                    $('#edit-field-selectall').closest('.form-item').show();
                }
                else {
                    $('#edit-field-selectall').attr('checked', false).closest('.form-item').hide();
                }
            }).trigger("change");

        }
    };

})(jQuery)