<?php


/**
 *  Implements hook_form_alter().
 */
function entityreference_selectall_form_alter(&$form, &$form_state, $form_id) {

  $entities = entity_get_info();

  if(!empty($form['#entity_type']) && !empty($form['#bundle'])
    && (!empty($form[$entities[$form['#entity_type']]['entity keys']['id']]))) {


    $addValidateSubmit = false;
    $fields_info = field_info_instances($form['#entity_type'], $form['#bundle']);

    foreach ($fields_info as $field_name => $value) {
      $field_info = field_info_field($field_name);
      $type       = $field_info['type'];

      if(!empty($field_info['selectall'])) {
        drupal_add_js(drupal_get_path('module', 'entityreference_selectall') . '/entityreference_selectall.js');
        drupal_add_css(drupal_get_path('module', 'entityreference_selectall') . '/entityreference_selectall.css');
        $addValidateSubmit = true;

        $status = 0;
        if(!empty($form_state['node']->vid)) {
          $status = db_select('entityreference_selectall_refs', 'r')
            ->condition('field_name', $field_name)
            ->condition('entity_type', $form['#entity_type'])
            ->condition('entity_id', $form_state['node']->vid)
            ->fields('r', array('status'))
            ->execute()
            ->fetchField();
        }

        $form[$field_name]['checkall'] = array(
          '#type' => 'checkbox',
          '#title' => t('Select all'),
          '#default_value' => $status,
          '#attributes' => array(
            'class' => array('entityreference_selectall'),
          ),
          '#prefix' => '<div class="entityreference_selectall_wrapper">',
          '#suffix' => '</div>'
        );
      }
    }

    if($addValidateSubmit) {
      $form['actions']['submit']['#validate'][]   = 'entityreference_selectall_entity_form_validate';
      $form['actions']['submit']['#submit'][]     = 'entityreference_selectall_entity_form_submit';
    }
  }

  switch($form_id) {
    case 'field_ui_field_edit_form':
      if($form['#field']['type'] == 'entityreference') {

        // add the selectall setting checkbox to the field settings form.
        $form['field']['selectall'] = array(
          '#type' => 'checkbox',
          '#title' => t("Add a 'Select all' checkbox"),
          '#weight' => 0,
          '#default_value' => !empty($form['#field']['selectall']) ? $form['#field']['selectall'] : 0,
        );

        drupal_add_js(drupal_get_path('module', 'entityreference_selectall') . '/entityreference_selectall.js');
        drupal_add_css(drupal_get_path('module', 'entityreference_selectall') . '/entityreference_selectall.css');
        $form['#submit'][] = 'entityreference_selectall_settings_form_submit';
      }
      break;
  }
}


/**
 * Custom submit function for the settings form.
 */
function entityreference_selectall_settings_form_submit($form, &$form_state) {

  $instance = $form_state['values']['instance'];
  $field = $form_state['values']['field'];

  $field_source = field_info_field($instance['field_name']);
  $field = array_merge($field_source, $field);
  field_update_field($field);
}


/**
 * Implements hook_entity_load().
 */
function entityreference_selectall_entity_load($entities, $type) {

  $entity_info = entity_get_info($type);
  foreach($entities as &$entity) {

    $bundle_name = $type;
    if(!empty($entity_info['entity keys']['bundle'])) {
      $bundle_name = $entity->{$entity_info['entity keys']['bundle']};
    }

    $field_instances = field_info_instances($type, $bundle_name);
    foreach($field_instances as $field_name => $field_instance) {
      $field_info = field_info_field($field_name);

      if(!empty($field_info['selectall']) && $field_info['selectall']) {
        $settings = entityreference_selectall_instance_values($field_name, $type, $entity->{$entity_info['entity keys']['id']});
        $entity->{$field_name}['selectall'] = is_array($settings) ? 1 : 0;
      }
    }
  }
}


/**
 * Custom validate function for the entity form.
 */
function entityreference_selectall_entity_form_validate($form, &$form_state) {

  $entity_type = $form['#entity_type'];
  $entity_info = entity_get_info($entity_type);

  $bundle = $entity_type;
  if(!empty($entity_info['entity keys']['bundle'])) {
    $bundle = $form['#entity']->{$entity_info['entity keys']['bundle']};
  }

  $field_instances = field_info_instances($entity_type, $bundle);

  foreach($field_instances as $field_name => $field_instance) {
    if(isset($form_state['values'][$field_name]['checkall']) && $form_state['values'][$field_name]['checkall']) {
      $field_info = field_info_field($field_name);
      $allrefs = entityreference_selectall_all_refs($field_info['settings']['target_type'], array_keys($field_info['settings']['handler_settings']['target_bundles']));

      foreach($allrefs as $ref) {
        $items[]['target_id'] = $ref;
      }

      $form_state['values'][$field_name]['und'] = $items;

    }
  }
}


/**
 * Custom submit function for the entity form.
 */
function entityreference_selectall_entity_form_submit($form, &$form_state) {

  $entity_type = $form['#entity_type'];
  $entity_info = entity_get_info($entity_type);

  $bundle = $entity_type;
  if(!empty($entity_info['entity keys']['bundle'])) {
    $bundle = $form['#entity']->{$entity_info['entity keys']['bundle']};
  }

  $bundle_fields = field_info_instances($entity_type, $bundle);

  foreach(array_keys($bundle_fields) as $field_name) {

    if(isset($form_state['values'][$field_name]['checkall'])) {

      $keys = array(
        'field_name' => $field_name,
        'entity_type' => $entity_type,
        'entity_id' => $form_state['node']->vid,
      );

      $fields = array(
        'field_name' => $field_name,
        'entity_type' => $entity_type,
        'entity_id' => $form_state['node']->vid,
        'status' => $form_state['values'][$field_name]['checkall'],
      );

      db_merge('entityreference_selectall_refs')
        ->key($keys)
        ->fields($fields)
        ->execute();

    }
  }
}


/**
 * Implements hook_entity_insert().
 */
function entityreference_selectall_entity_insert($entity, $type) {

  $entity_info = entity_get_info($type);

  $bundle = $type;
  if(!empty($entity_info['entity keys']['bundle'])) {
    $bundle = $entity->{$entity_info['entity keys']['bundle']};
  }

  $batch_operations = array();
    $refs = entityreference_selectall_get_referring_fields($type, $bundle);
  foreach($refs as $field_name) {
    $entities = entityreference_selectall_get_field_entities($field_name);

    foreach($entities as $selectall_ref) {
      $batch_operations[] = array('entityreference_selectall_batch_process', array($field_name, $selectall_ref));
    }
  }

  if(count($batch_operations)) {
    $batch = array(
      'operations' => $batch_operations,
      'title' => t('Processing all references'),
      'init_message' => t('References batch is starting.'),
      'progress_message' => t('Processed @current out of @total references.'),
      'error_message' => t('References batch has encountered an error.'),
    );
    batch_set($batch);
  }
}




/* HELPER FUNCTIONS */


function entityreference_selectall_get_field_entities($field_name) {

  $query = db_select('entityreference_selectall_refs', 'f');
  $query
    ->fields('f')
    ->condition('f.field_name', $field_name)
    ->condition('f.status', 1);

  return $query->execute();
}


function entityreference_selectall_instance_values($field_name, $entity_type, $entity_id) {

  $query = db_select('entityreference_selectall_refs', 'f');
  $query
    ->fields('f')
    ->condition('f.field_name', $field_name)
    ->condition('f.entity_type', $entity_type)
    ->condition('f.entity_id', $entity_id)
    ->condition('f.status', 1);

  return $query->execute()->fetchAssoc();
}


function entityreference_selectall_all_refs($entity_type, $bundles) {

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', $entity_type);

  foreach($bundles as $bundle) {
    $query->entityCondition('bundle', $bundle);
  }

  $result = $query->execute();
  if (isset($result[$entity_type])) {
    return array_keys($result[$entity_type]);
  }
}


function entityreference_selectall_get_referring_fields($entity_type, $bundle) {

  $refs = array();
  $fields = field_info_fields();
  foreach($fields as $field_name => $field) {
    if($field['type'] == 'entityreference' && isset($field['selectall']) && $field['selectall'] == 1) {

      if($field['settings']['target_type'] == $entity_type
        && in_array($bundle, $field['settings']['handler_settings']['target_bundles'])) {
        $refs[] = $field_name;
      }
    }
  }

  return count($refs) ? $refs : array();
}


function entityreference_selectall_batch_process($field_name, $selectall_ref) {
  $linking_entity = entity_load_single($selectall_ref->entity_type, $selectall_ref->entity_id);

  $items = array();
  $field_info = field_info_field($field_name);
  $allrefs = entityreference_selectall_all_refs($field_info['settings']['target_type'], array_keys($field_info['settings']['handler_settings']['target_bundles']));

  foreach($allrefs as $ref) {
    $items[]['target_id'] = $ref;
  }

  $linking_entity->{$field_name}[LANGUAGE_NONE] = $items;

  entity_save($selectall_ref->entity_type, $linking_entity);
}